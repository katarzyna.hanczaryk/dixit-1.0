﻿using System.ComponentModel.DataAnnotations;

namespace Dixit.Model
{
    public class Card
    {
        [Key]
        public int Id { get; set; }
        public string FileName { get; set; }
    }
}
