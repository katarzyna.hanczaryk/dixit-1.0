﻿using System.ComponentModel.DataAnnotations;

namespace Dixit.Model
{
    public class Round
    {
        [Key]
        public int Id { get; set; }
        public int StorytellerId { get; set; }
    }
}
