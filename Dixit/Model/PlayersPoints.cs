﻿namespace Dixit.Model
{
    public class PlayersPoints
    {
        public string PlayerName { get; set; }
        public int RoundPoints { get; set; }
        public int GamePoints { get; set; }
    }
}
