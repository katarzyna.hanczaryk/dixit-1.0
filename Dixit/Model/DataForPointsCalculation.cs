﻿using Microsoft.EntityFrameworkCore;

namespace Dixit.Model
{
    [Keyless]
    public class DataForPointsCalculation
    {
        // is it fine not to have Id?
        public int GameId { get; set; }
        public int RoundId { get; set; }
        public int StorytellerId { get; set; }
        public int CardId { get; set; }
        public int? SelectedByPlayer { get; set; }
        public string Name { get; set; }
        public int? GuessedCardId { get; set; }
    }
}
