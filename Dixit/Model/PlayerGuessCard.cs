﻿using System.ComponentModel.DataAnnotations;

namespace Dixit.Model
{
    public class PlayerGuessCard
    {
        [Key]
        public int Id { get; set; }
        public int RoundChosenCardId { get; set; }
        public int PlayerId { get; set; }
    }
}
