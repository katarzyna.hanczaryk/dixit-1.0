﻿using System.ComponentModel.DataAnnotations;

namespace Dixit.Model
{
    public class RoundChosenCard
    {
        [Key]
        public int Id { get; set; }
        public int RoundId { get; set; }
        public int CardId { get; set; }
    }
}
