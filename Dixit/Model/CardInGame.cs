﻿using System.ComponentModel.DataAnnotations;

namespace Dixit.Model
{
    public class CardInGame
    {
        [Key]
        public int Id { get; set; }
        public int CardId { get; set; }
        public int PlayerId { get; set; }
    }
}
