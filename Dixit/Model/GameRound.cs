﻿using System.ComponentModel.DataAnnotations;

namespace Dixit.Model
{
    public class GameRound
    {
        [Key]
        public int Id { get; set; }
        public int GameId { get; set; }
        public int RoundId { get; set; }
    }
}
