using Dixit.Common;
using Dixit.Model;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;

namespace Dixit.Pages
{
    public class WaitForHintModel : Helper<WaitForHintModel>
    {
        [BindProperty]
        public IEnumerable<Card> Cards { get; set; }
        [BindProperty]
        public IEnumerable<PlayersPoints> PlayersPoints { get; set; }
        public WaitForHintModel(ApplicationDbContext db, ILoggerFactory loggerFactory, IOptions<GameRules> app)
            : base(db, loggerFactory, app) { }
        public IActionResult OnGet(int playerId, int gameId, int roundId)
        {
            try
            {
                PlayersPoints = RetrievePlayersRoundAndGamePoints(gameId);
                Cards = RetrieveAllPlayerCards(playerId, gameId);
                if (RetrieveHint(roundId) == null) return Page();
                return RedirectToPage("HintDisplayToPlayer", new { playerId, gameId, roundId });
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.StackTrace, "Error while retrieving storyteller's hint");
                return StatusCode(500, "Error while retrieving storyteller's hint");
            }
        }
    }
}
