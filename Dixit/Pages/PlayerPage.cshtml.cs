using Dixit.Common;
using Dixit.Model;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using System;
using System.Threading.Tasks;

namespace Dixit.Pages
{
    public class PlayerPageModel : Helper<PlayerPageModel>
    {
        public PlayerPageModel(ApplicationDbContext db, ILoggerFactory loggerFactory, IOptions<GameRules> app)
            : base(db, loggerFactory, app) { }

        public async Task<IActionResult> OnGet(int playerId, int gameId, int roundId)
        {
            try
            {
                await GenerateRandomPlayerCardsAsync(playerId, gameId);
                if (playerId == GetCurrentStorytellerId(roundId))
                    return RedirectToPage("StorytellerChooseCard", new { playerId, gameId, roundId });
                return RedirectToPage("WaitForHint", new { playerId, gameId, roundId });
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.StackTrace, "Error assigning player's cards");
                return StatusCode(500, "Error retrieving player's cards");
            }
        }
    }
}
