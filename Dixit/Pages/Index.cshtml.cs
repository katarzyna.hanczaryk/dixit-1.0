﻿using Dixit.Common;
using Dixit.Model;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Threading.Tasks;

namespace Dixit.Pages
{
    public class IndexModel : Helper<IndexModel>
    {
        public IndexModel(ApplicationDbContext db, ILoggerFactory loggerFactory) : base(db, loggerFactory)
        {
        }

        [BindProperty]
        public Player Player { get; set; }

        public async Task<IActionResult> OnPost()
        {
            if (ModelState.IsValid)
            {
                try
                {
                    int playerId = await AddNewPlayerAsync(Player.Name);
                    return RedirectToPage("ReadyToPlay", new { playerId });
                }
                catch (Exception ex)
                {
                    _logger.LogError(ex.StackTrace, "Error adding new player to db");
                    return StatusCode(500, "Error while adding a new player");
                }
            }
            else
            {
                return Page();
            }
        }
    }
}
