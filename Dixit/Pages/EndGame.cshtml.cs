using Dixit.Common;
using Dixit.Model;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Dixit.Pages
{
    public class EndGameModel : Helper<EndGameModel>
    {
        [BindProperty]
        public IEnumerable<PlayersPoints> PlayersPoints { get; set; }
        [BindProperty]
        public IEnumerable<string> WinnerName { get; set; }
        public EndGameModel(ApplicationDbContext db, ILoggerFactory loggerFactory, IOptions<GameRules> app)
            : base(db, loggerFactory, app) { }
        public void OnGet(int gameId)
        {
            try
            {
                PlayersPoints = RetrievePlayersRoundAndGamePoints(gameId);
                int maxPoints = PlayersPoints.Select(n => n.GamePoints).Max();
                WinnerName = PlayersPoints.Where(m => m.GamePoints == PlayersPoints
                                                        .Select(n => n.GamePoints)
                                                        .Max())
                                          .Select(k => k.PlayerName);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.StackTrace, "Error retrieving players' points and the winner name");
                throw ex;
            }
        }
    }
}
