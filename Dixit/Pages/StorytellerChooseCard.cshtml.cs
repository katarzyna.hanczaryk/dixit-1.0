using Dixit.Common;
using Dixit.Model;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Dixit.Pages
{
    public class StorytellerChooseCardModel : Helper<StorytellerChooseCardModel>
    {
        [BindProperty]
        public IEnumerable<Card> Cards { get; set; }
        [BindProperty]
        public IEnumerable<PlayersPoints> PlayersPoints { get; set; }
        public StorytellerChooseCardModel(ApplicationDbContext db, ILoggerFactory loggerFactory, IOptions<GameRules> app)
            : base(db, loggerFactory, app) { }
        public void OnGet(int playerId, int gameId, int roundId)
        {
            try
            {
                Cards = RetrieveAllPlayerCards(playerId, gameId);
                PlayersPoints = RetrievePlayersRoundAndGamePoints(gameId);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.StackTrace, "Error while retriving data to display to storyteller");
            }
        }

        public async Task<ActionResult> OnPost(int playerId, int gameId, int roundId, int id)
        {
            try
            {
                // id => id of the chosen card
                await AddRoundChosenCardAsync(id, roundId);
                return RedirectToPage("AddDescription",
                    new { playerId, gameId, roundId, id });
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.StackTrace, "Error while add round chosen card");
                return StatusCode(500, "Error while add round chosen card");
            }
        }
    }
}
