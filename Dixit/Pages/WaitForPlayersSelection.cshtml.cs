using Dixit.Common;
using Dixit.Model;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Dixit.Pages
{
    public class WaitForPlayersSelectionModel : Helper<WaitForPlayersSelectionModel>
    {
        [BindProperty]
        public IEnumerable<string> RemainingCards { get; set; }
        [BindProperty]
        public string ChosenCard { get; set; }
        [BindProperty]
        public string Hint { get; set; }
        [BindProperty]
        public IEnumerable<PlayersPoints> PlayersPoints { get; set; }
        public WaitForPlayersSelectionModel(ApplicationDbContext db, ILoggerFactory loggerFactory,
            IOptions<GameRules> app) : base(db, loggerFactory, app) { }
        public async Task<IActionResult> OnGet(int playerId, int gameId, int roundId, int id)
        {
            try
            {
                PlayersPoints = RetrievePlayersRoundAndGamePoints(gameId);
                int playersNumber = await RetrievePlayerNumberAsync(gameId);
                int totalRoundChosenCards = await RetrieveTotalRoundChosenCardsAsync(roundId);
                if (totalRoundChosenCards < playersNumber)
                {
                    ChosenCard = await RetrievePlayerChosenCardAsync (id);
                    RemainingCards = RetrieveRemainingCardAddresses(playerId);
                    Hint = RetrieveHint(roundId);
                    return Page();
                }
                return RedirectToPage("ShowOtherPlayersSelection", new { playerId, gameId, roundId, id });
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.StackTrace, "Error while retrieving players' selection");
                return StatusCode(500, "Error while retrieving players' selection");
            }
        }
    }
}
