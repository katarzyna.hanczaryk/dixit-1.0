using Dixit.Common;
using Dixit.Model;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using System;
using System.Threading.Tasks;

namespace Dixit.Pages
{
    public class WaitForMorePlayersModel : Helper<WaitForMorePlayersModel>
    {
        public WaitForMorePlayersModel(ApplicationDbContext db, ILoggerFactory loggerFactory, IOptions<GameRules> app)
            : base(db, loggerFactory, app) { }
        public async Task<IActionResult> OnGet(int gameId, int playerId, int roundId)
        {
            try
            {
                int playersInGame = await RetrievePlayerNumberAsync(gameId);
                if (playersInGame == _appSettings.Value.RequiredPlayersNumber)
                    return RedirectToPage("PlayerPage", new { playerId, gameId, roundId });
                return Page();
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.StackTrace, "Error while retrieving data about other players.");
                return StatusCode(500, "Error while retrieving data about other players.");
            }
        }
    }
}
