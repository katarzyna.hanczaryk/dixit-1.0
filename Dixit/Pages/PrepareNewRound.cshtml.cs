using Dixit.Common;
using Dixit.Model;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using System;
using System.Threading.Tasks;

namespace Dixit.Pages
{
    public class PrepareNewRoundModel : Helper<PrepareNewRoundModel>
    {
        public PrepareNewRoundModel(ApplicationDbContext db, ILoggerFactory loggerFactory, IOptions<GameRules> app)
            : base(db, loggerFactory, app) { }
        public async Task<IActionResult> OnGet(int playerId, int gameId)
        {
            try
            {
                await AssignPlayerMissingCardsAsync(playerId, gameId);
                int roundId = await AddNewRoundAndChangeStorytellerAsync(playerId, gameId);
                int currentStorytellerId = GetCurrentStorytellerId(roundId);
                if (playerId == currentStorytellerId)
                {
                    return RedirectToPage("StorytellerChooseCard", new { playerId, gameId, roundId });
                }
                return RedirectToPage("WaitForHint", new { playerId, gameId, roundId });
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.StackTrace, "Error while preparing a new round");
                return StatusCode(500, "Error while preparing a new round");
            }
        }
    }
}
