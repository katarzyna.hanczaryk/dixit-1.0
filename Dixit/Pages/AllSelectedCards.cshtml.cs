using Dixit.Common;
using Dixit.Model;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Dixit.Pages
{
    public class AllSelectedCardsModel : Helper<AllSelectedCardsModel>
    {
        [BindProperty]
        public IEnumerable<string> RemainingCards { get; set; }
        [BindProperty]
        public string Hint { get; set; }
        [BindProperty]
        public IEnumerable<Card> AllSelectedCards { get; set; }
        [BindProperty]
        public IEnumerable<PlayersPoints> PlayersPoints { get; set; }
        public AllSelectedCardsModel(ApplicationDbContext db, ILoggerFactory loggerFactory, IOptions<GameRules> app)
                : base(db, loggerFactory, app) { }
        public void OnGet(int playerId, int gameId, int roundId)
        {
            try
            {
                PlayersPoints = RetrievePlayersRoundAndGamePoints(gameId);
                RemainingCards = RetrieveRemainingCardAddresses(playerId);
                Hint = RetrieveHint(roundId);
                AllSelectedCards = RetrieveRoundSelectedCards(roundId);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.StackTrace, "Error retrieving data for player's info");
            }
        }

        //id of the guessed card
        public async Task<ActionResult> OnPost(int id, int playerId, int gameId, int roundId)
        {
            try
            {
                if (await IsOwnCardSelectedAsync(id, playerId))
                    return RedirectToPage("AllSelectedCards", new { playerId, gameId, roundId });
                await AddPlayerGuessCardAsync(playerId, roundId, id);
                return RedirectToPage("WaitForRoundResults", new { playerId, gameId, id, roundId });
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.StackTrace, "Error while adding the player's guess");
                return StatusCode(500, "Error while adding the player's guess");
            }
        }
    }
}
