using Dixit.Common;
using Dixit.Model;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Dixit.Pages
{
    public class AddDescriptionModel : Helper<AddDescriptionModel>
    {
        [BindProperty]
        public IEnumerable<string> RemainingCards { get; set; }
        [BindProperty]
        public string ChosenCard { get; set; }
        [BindProperty]
        public string CardDescription { get; set; }
        [BindProperty]
        public IEnumerable<PlayersPoints> PlayersPoints { get; set; }
        public AddDescriptionModel(ApplicationDbContext db, ILoggerFactory loggerFactory, IOptions<GameRules> app)
            : base(db, loggerFactory, app) { }

        public async Task OnGet(int playerId, int gameId, int id)
        {
            try
            {
                ChosenCard = await RetrievePlayerChosenCardAsync(id);
                RemainingCards = RetrieveRemainingCardAddresses(playerId);
                PlayersPoints = RetrievePlayersRoundAndGamePoints(gameId);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.StackTrace, "Error retrieving cards or points");
                throw ex;
            }
        }

        public async Task<ActionResult> OnPost(int playerId, int gameId, int roundId, int id)
        {
            try
            {
                await AddCardDescriptionAsync(roundId, CardDescription);
                return RedirectToPage("WaitForPlayersSelection", new { playerId, gameId, roundId, id });
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.StackTrace, "Error adding storyteller's hint");
                return StatusCode(500, "Error adding storyteller's hint");
            }
        }
    }
}
