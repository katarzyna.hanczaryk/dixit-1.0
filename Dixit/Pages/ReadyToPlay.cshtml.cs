using Dixit.Common;
using Dixit.Model;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using System;
using System.Threading.Tasks;

namespace Dixit.Pages
{
    public class ReadyToPlayModel : Helper<ReadyToPlayModel>
    {
        public ReadyToPlayModel(ApplicationDbContext db, ILoggerFactory loggerFactory, IOptions<GameRules> app)
            : base(db, loggerFactory, app) { }

        public async Task<IActionResult> OnPost(int playerId)
        {
            try
            {
                var gameAndRound = await AssignToGameAsync(playerId);
                int gameId = gameAndRound.gameId;
                int roundId = gameAndRound.roundId;
                return RedirectToPage("WaitForMorePlayers", new { playerId, gameId, roundId });
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.StackTrace, "Error while assigning a player to a game");
                return StatusCode(500, "Error while assigning a player to a new game");
            }
        }
    }
}
