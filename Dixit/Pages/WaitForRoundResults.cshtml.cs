using Dixit.Common;
using Dixit.Model;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Dixit.Pages
{
    public class WaitForRoundResultsModel : Helper<WaitForRoundResultsModel>
    {
        [BindProperty]
        public IEnumerable<string> RemainingCards { get; set; }
        [BindProperty]
        public string Hint { get; set; }
        [BindProperty]
        public IEnumerable<Card> AllSelectedCards { get; set; }
        [BindProperty]
        public IEnumerable<PlayersPoints> PlayersPoints { get; set; }
        [BindProperty]
        public string PlayerGuessedCardAddress { get; set; }
        public int PlayersNumber { get; set; }

        public WaitForRoundResultsModel(ApplicationDbContext db, ILoggerFactory loggerFactory,
            IOptions<GameRules> app) : base(db, loggerFactory, app) { }

        public async Task<IActionResult> OnGet(int playerId, int roundId, int gameId, int id)
        {
            try
            {
                PlayersNumber = await RetrievePlayerNumberAsync(gameId);
                int totalRoundGuesses = await RetrieveRoundGuessesNumberAsync(roundId);
                // waiting for missing players' guesses
                if (totalRoundGuesses < PlayersNumber - 1)
                {
                    PlayersPoints = RetrievePlayersRoundAndGamePoints(gameId);
                    RemainingCards = RetrieveRemainingCardAddresses(playerId);
                    Hint = RetrieveHint(roundId);
                    AllSelectedCards = RetrieveRoundSelectedCards(roundId);
                    PlayerGuessedCardAddress = RetrievePlayerGuessedCardAddress(id);
                    return Page();
                }
                if (CalculateGameMaxPointsNumber(gameId) < _appSettings.Value.WinningPoints)
                {
                    // int currentStorytellerId = GetCurrentStorytellerId(roundId);
                    return RedirectToPage("PrepareNewRound", new { gameId, playerId, roundId });
                }
                return RedirectToPage("EndGame", new { playerId, gameId, roundId });

            }
            catch (Exception ex)
            {
                _logger.LogError(ex.StackTrace, "Error while processing round results");
                return StatusCode(500, "Error while processing round results");
            }
        }
    }
}
