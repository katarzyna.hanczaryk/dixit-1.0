using Dixit.Common;
using Dixit.Model;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Dixit.Pages
{
    public class PlayerCardChoiceModel : Helper<PlayerCardChoiceModel>
    {
        [BindProperty]
        public IEnumerable<string> RemainingCards { get; set; }
        [BindProperty]
        public string Hint { get; set; }
        [BindProperty]
        public string CardChosen { get; set; }
        [BindProperty]
        public IEnumerable<PlayersPoints> PlayersPoints { get; set; }
        public PlayerCardChoiceModel(ApplicationDbContext db, ILoggerFactory loggerFactory, IOptions<GameRules> app)
            : base(db, loggerFactory, app) { }
        public async Task<IActionResult> OnGet(int playerId, int id, int gameId, int roundId)
        {
            try
            {
                int currentSelectedCardsNumber = RetrieveRoundSelectedCards(roundId).Count();
                if (currentSelectedCardsNumber < await RetrievePlayerNumberAsync(gameId))
                {
                    PlayersPoints = RetrievePlayersRoundAndGamePoints(gameId);
                    RemainingCards = RetrieveRemainingCardAddresses(playerId);
                    Hint = RetrieveHint(roundId);
                    CardChosen = await RetrievePlayerChosenCardAsync(id);
                    return Page();
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.StackTrace, "Error while player adding a card choice");
                return StatusCode(500, "Error adding the player selection");
            }
            return RedirectToPage("AllSelectedCards", new { playerId, gameId, roundId });
        }
    }
}
