using Dixit.Common;
using Dixit.Model;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Dixit.Pages
{
    public class HintDisplayToPlayerModel : Helper<HintDisplayToPlayerModel>
    {
        [BindProperty]
        public IEnumerable<Card> Cards { get; set; }
        [BindProperty]
        public string Hint { get; set; }
        [BindProperty]
        public IEnumerable<PlayersPoints> PlayersPoints { get; set; }
        public HintDisplayToPlayerModel(ApplicationDbContext db, ILoggerFactory loggerFactory, IOptions<GameRules> app)
            : base(db, loggerFactory, app) { }
        public void OnGet(int gameId, int playerId, int roundId)
        {
            try
            {
                PlayersPoints = RetrievePlayersRoundAndGamePoints(gameId);
                Cards = RetrieveAllPlayerCards(playerId, gameId);
                Hint = RetrieveHint(roundId);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.StackTrace, "Error retrieving info for a hint displaying page");
                throw ex;
            }
        }

        public async Task<ActionResult> OnPost(int playerId, int gameId, int id, int roundId)
        {
            try
            {
                // id - id of the chosen card   
                await AddRoundChosenCardAsync(id, roundId);
                return RedirectToPage("PlayerCardChoice", new { playerId, gameId, id, roundId });
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.StackTrace, "Error adding player's selected card to db");
                return StatusCode(500, "Error saving player's card selection");
            }
        }
    }
}
