using Dixit.Common;
using Dixit.Model;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Dixit.Pages
{
    public class ShowOtherPlayersSelectionModel : Helper<ShowOtherPlayersSelectionModel>
    {
        [BindProperty]
        public string ChosenCard { get; set; }
        [BindProperty]
        public string Hint { get; set; }
        [BindProperty]
        public IEnumerable<Card> AllSelectedCards { get; set; }
        [BindProperty]
        public IEnumerable<PlayersPoints> PlayersPoints { get; set; }
        [BindProperty]
        public IEnumerable<string> RemainingCards { get; set; }
        public ShowOtherPlayersSelectionModel(ApplicationDbContext db, ILoggerFactory loggerFactory, IOptions<GameRules> app)
            : base(db, loggerFactory, app) { }
        public async Task<IActionResult> OnGet(int gameId, int roundId, int playerId, int id)
        {
            try
            {
                if (await IsNewRoundAddedAsync(roundId))
                    return RedirectToPage("WaitForRoundResults", new { playerId, roundId, gameId, id });

                RemainingCards = RetrieveRemainingCardAddresses(playerId);
                PlayersPoints = RetrievePlayersRoundAndGamePoints(gameId);
                Hint = RetrieveHint(roundId);
                ChosenCard = await RetrievePlayerChosenCardAsync (id);
                AllSelectedCards = await RetrieveAllSelectedCardsForStorytellerAsync(roundId);
                return Page();
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.StackTrace, "Error while retrieving selected cards");
                return StatusCode(500, "Error while retrieving selected cards");
            }
        }
    }
}
