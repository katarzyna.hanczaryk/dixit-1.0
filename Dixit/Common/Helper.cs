﻿using Dixit.Model;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Dixit.Common
{
    public abstract class Helper<TClass> : PageModel
    {
        protected readonly IOptions<GameRules> _appSettings;
        protected readonly ApplicationDbContext _db;
        protected readonly ILogger _logger;

        protected Helper(ApplicationDbContext db, ILoggerFactory loggerFactory)
        {
            _db = db;
            _logger = loggerFactory.CreateLogger<TClass>();
        }

        protected Helper(ApplicationDbContext db, ILoggerFactory loggerFactory, IOptions<GameRules> app)
        {
            _db = db;
            _logger = loggerFactory.CreateLogger<TClass>();
            _appSettings = app;
        }


        protected IEnumerable<string> RetrieveRemainingCardAddresses(int playerId)
        {
            bool selectPlayerCards(Card card) => _db.CardsInGame
                                                 .Where(cardInGame => cardInGame.PlayerId == playerId)
                                                 .Select(cardInGame => cardInGame.CardId)
                                                 .Contains(card.Id);

            var remainingCardsIds = _db.Cards
                                         .Where(selectPlayerCards)
                                         .Select(card => card.Id)
                                         .Except(_db.RoundChosenCards.Select(n => n.CardId));

            return _db.Cards.Where(m => remainingCardsIds
                                        .Contains(m.Id))
                                        .Select(n => n.FileName);
        }

        protected async Task GenerateRandomPlayerCardsAsync(int playerId, int gameId)
        {
            Random r = new Random(_appSettings.Value.Seed++);
            ISet<int> randomIds = new HashSet<int>(_appSettings.Value.TotalCards);
            IEnumerable<int> playerIds = GetTotalGamePlayerIds(gameId);
            IEnumerable<int> cardsInGameIds = _db.CardsInGame
                                                .Where(m => playerIds.Contains(m.PlayerId))
                                                .Select(n => n.CardId);
            while (randomIds.Count() < _appSettings.Value.TotalCards)
            {
                int generatedRandomId = r.Next(1, _db.Cards.Count());
                // to make sure that all assigned cards are unique for the game
                if (!cardsInGameIds.Contains(generatedRandomId))
                    randomIds.Add(generatedRandomId);
            }
            IEnumerable<Card> cards = _db.Cards.Where(m => randomIds.Contains(m.Id));

            await _db.CardsInGame.AddRangeAsync(cards.Select(n => new CardInGame()
            {
                CardId = n.Id,
                PlayerId = playerId
            }));
            await _db.SaveChangesAsync();
        }

        protected IEnumerable<Card> RetrieveAllPlayerCards(int playerId, int gameId)
        {
            IEnumerable<int> totalRounds = _db.GameRounds
                                             .Where(m => m.GameId == gameId)
                                             .Select(n => n.RoundId);
            IEnumerable<int> totalGameUsedCards = _db.RoundChosenCards
                                                    .Where(m => totalRounds.Contains(m.RoundId))
                                                    .Select(n => n.CardId);

            IEnumerable<int> totalCardIds = _db.CardsInGame
                     .Where(m => m.PlayerId == playerId)
                     .Select(n => n.CardId)
                     .Except(totalGameUsedCards);

            return _db.Cards
                     .Where(m => totalCardIds.Contains(m.Id));
        }

        protected string RetrieveHint(int roundId)
        {
            CardDescription currentHint = _db.CardDescriptions
                                             .Where(n => n.RoundId == roundId)
                                             .FirstOrDefault();
            return currentHint?.Description;
        }

        protected async Task AddRoundChosenCardAsync(int chosenCardId, int roundId)
        {
            await _db.RoundChosenCards.AddAsync(new RoundChosenCard()
            {
                CardId = chosenCardId,
                RoundId = roundId
            });
            await _db.SaveChangesAsync();
        }

        protected async Task<string> RetrievePlayerChosenCardAsync(int chosenCardId)
        {
            return await _db.Cards
                            .Where(m => m.Id == chosenCardId)
                            .Select(n => n.FileName)
                            .FirstAsync();
        }

        protected IEnumerable<Card> RetrieveRoundSelectedCards(int roundId)
        {
            return _db.Cards
                      .Where(m => _db.RoundChosenCards
                            .Where(m => m.RoundId == roundId)
                            .Select(n => n.CardId)
                      .Contains(m.Id));
        }

        protected async Task<int> RetrievePlayerNumberAsync(int gameId)
        {
            return await _db.GamePlayers
                      .Where(m => m.GameId == gameId)
                      .CountAsync();
        }

        protected async Task<Card> GetStorytellerCardAsync(int roundId)
        {
            var roundChosenCardIds = _db.RoundChosenCards
                                        .Where(m => m.RoundId == roundId)
                                        .Select(m => m.CardId);

            var roundStorytellerCard = await _db.CardsInGame
                                            .Where(m => m.PlayerId == _db.Rounds
                                                                        .Where(m => m.Id == roundId)
                                                                        .First()
                                                                        .StorytellerId
                                                                        && roundChosenCardIds.Contains(m.CardId))
                                            .FirstAsync();
            int roundStorytellerCardId = roundStorytellerCard.CardId;
            return await _db.Cards.Where(m => m.Id == roundStorytellerCardId)
                                              .FirstAsync();
        }

        protected int GetCurrentStorytellerId(int roundId)
        {
            return _db.Rounds
                      .Where(m => m.Id == roundId)
                      .First()
                      .StorytellerId;
        }

        protected IEnumerable<int> GetTotalGamePlayerIds(int gameId)
        {
            return _db.GamePlayers
                     .Where(m => m.GameId == gameId)
                     .Select(n => n.PlayerId);
        }

        protected async Task<int> RetrieveRoundGuessesNumberAsync(int roundId)
        {
            IEnumerable<int> currentRoundChosenCardsIds = _db.RoundChosenCards
                                                            .Where(m => m.RoundId == roundId)
                                                            .Select(n => n.Id);
            return await _db.PlayerGuessCards
                     .Where(m => currentRoundChosenCardsIds.Contains(m.RoundChosenCardId))
                     .CountAsync();
        }

        protected int GenerateUniqueRandomCardId(int gameId)
        {
            Random r = new Random(_appSettings.Value.Seed++);
            int uniqueRandomCardId = 0;
            while (uniqueRandomCardId == 0)
            {
                int generatedRandomId = r.Next(1, 85);
                // to make sure that all assigned cards are unique for the game
                IEnumerable<int> playerIds = GetTotalGamePlayerIds(gameId);
                IEnumerable<int> cardsInGameIds = _db.CardsInGame
                                                    .Where(m => playerIds.Contains(m.PlayerId))
                                                    .Select(n => n.CardId);
                if (!cardsInGameIds.Contains(generatedRandomId))
                    uniqueRandomCardId = generatedRandomId;
            }
            return uniqueRandomCardId;
        }

        protected IEnumerable<PlayersPoints> RetrievePlayersRoundAndGamePoints(int gameId)
        {
            List<int> players = GetTotalGamePlayerIds(gameId).ToList();
            IEnumerable<DataForPointsCalculation> dataForPointsCalculations = _db.DataForPointsCalculation.Where(m => m.GameId == gameId);
            IEnumerable<(int playerId, int totalPoints)> totalPoints = CalculateTotalPoints(dataForPointsCalculations);
            int lastRoundId = dataForPointsCalculations.GroupBy(m => m.RoundId)
                                                       .OrderByDescending(n => n.Key)
                                                       .SkipWhile(group => group.Count() < players.Count() || group.Where(k => k.GuessedCardId == null).Count() > 1)
                                                       .Select(l => l.Key)
                                                       .FirstOrDefault();
            IEnumerable<DataForPointsCalculation> lastRoundData = dataForPointsCalculations.Where(m => m.RoundId == lastRoundId);
            IEnumerable<(int playerId, int roundPoints)> lastRoundPoints = CalculateRoundPoints(lastRoundData);
            return
                players.Select(m => new PlayersPoints()
                {
                    PlayerName = _db.Players.Where(n => n.Id == m)
                                           .Select(k => k.Name)
                                           .FirstOrDefault(),
                    RoundPoints = lastRoundPoints.Where(n => n.playerId == m)
                                                 .Select(k => k.roundPoints)
                                                 .FirstOrDefault(),
                    GamePoints = totalPoints.Where(n => n.playerId == m)
                                            .Select(k => k.totalPoints)
                                            .FirstOrDefault()
                });
        }

        protected int CalculateGameMaxPointsNumber(int gameId)
        {
            IEnumerable<DataForPointsCalculation> dataForPointsCalculations = _db.DataForPointsCalculation
                                                                                .Where(m => m.GameId == gameId);
            IEnumerable<(int playerId, int totalPoints)> playersTotalPoints = CalculateTotalPoints(dataForPointsCalculations);
            return playersTotalPoints.Max(m => m.totalPoints);
        }

        protected async Task AddCardDescriptionAsync(int roundId, string cardDescription)
        {
            await _db.CardDescriptions.AddAsync(new CardDescription
            {
                Description = cardDescription,
                RoundId = roundId
            });
            await _db.SaveChangesAsync();
        }

        protected async Task AddPlayerGuessCardAsync(int playerId, int roundId, int id)
        {
            await _db.PlayerGuessCards.AddAsync(new PlayerGuessCard
            {
                PlayerId = playerId,
                RoundChosenCardId = _db.RoundChosenCards
                                            .Where(m => m.CardId == id
                                                         && m.RoundId == roundId)
                                            .First().Id
            });
            await _db.SaveChangesAsync();
        }

        protected async Task<int> AddNewPlayerAsync(string name)
        {
            var id = new SqlParameter()
            {
                ParameterName = "@Identity",
                SqlDbType = System.Data.SqlDbType.Int,
                Direction = System.Data.ParameterDirection.Output
            };
            await _db.Database.ExecuteSqlInterpolatedAsync($"EXEC [dbo].[spAddNewPlayer] {name}, {id} OUTPUT");
            return (int)id.Value;
        }
        protected async Task AssignPlayerMissingCardsAsync(int playerId, int gameId)
        {
            await _db.CardsInGame
                     .AddAsync(new CardInGame
                     {
                         PlayerId = playerId,
                         CardId = GenerateUniqueRandomCardId(gameId)
                     });
            await _db.SaveChangesAsync();
        }

        protected async Task<int> AddNewRoundAndChangeStorytellerAsync(int playerId, int gameId)
        {
            var currentRoundId = new SqlParameter
            {
                ParameterName = "@CurrentRoundId",
                SqlDbType = System.Data.SqlDbType.Int,
                Direction = System.Data.ParameterDirection.Output
            };
            await _db.Database
                     .ExecuteSqlInterpolatedAsync($@"EXEC [dbo].[spAddNewRoundAndNewStoryteller] {playerId}, 
                                                    {gameId}, {currentRoundId} OUTPUT");
            return (int)currentRoundId.Value;
        }

        protected async Task<(int gameId, int roundId)> AssignToGameAsync(int playerId)
        {
            var gameId = new SqlParameter
            {
                ParameterName = "@GameIdToAssign",
                SqlDbType = System.Data.SqlDbType.Int,
                Direction = System.Data.ParameterDirection.Output
            };
            var roundId = new SqlParameter
            {
                ParameterName = "@RoundId",
                SqlDbType = System.Data.SqlDbType.Int,
                Direction = System.Data.ParameterDirection.Output
            };
            await _db.Database
                .ExecuteSqlInterpolatedAsync($"EXEC [dbo].[spAssignPlayerToGame] {playerId}, {gameId} OUTPUT, {roundId} OUTPUT");
            return ((int)gameId.Value, (int)roundId.Value);
        }

        protected async Task<bool> IsNewRoundAddedAsync(int roundId)
        {
            return await _db.CardDescriptions
                            .Where(m => m.RoundId == roundId)
                            .AnyAsync();
        }

        protected async Task<IEnumerable<Card>> RetrieveAllSelectedCardsForStorytellerAsync(int roundId)
        {
            Card storytellerCard = await GetStorytellerCardAsync(roundId);
            return _db.Cards
                                  .Where(m => _db.RoundChosenCards
                                  .Where(m => m.RoundId == roundId &&
                                       m.CardId != storytellerCard.Id)
                                  .Select(n => n.CardId)
                                  .Contains(m.Id));
        }

        protected string RetrievePlayerGuessedCardAddress(int id)
        {
            return _db.Cards
                      .Where(m => m.Id == id)
                      .FirstOrDefault()
                      .FileName;
        }

        protected async Task<int> RetrieveTotalRoundChosenCardsAsync(int roundId)
        {
            return await _db.RoundChosenCards
                            .Where(m => m.RoundId == roundId)
                            .CountAsync();
        }

        protected async Task<bool> IsOwnCardSelectedAsync(int id, int playerId)
        {
            return await _db.CardsInGame
                            .Where(m => m.CardId == id && m.PlayerId == playerId)
                            .AnyAsync();
        }

        //to calculate points per a given roundId for all players
        private IEnumerable<(int playerId, int roundPoints)> CalculateRoundPoints(IEnumerable<DataForPointsCalculation> dataForRoundPoints)
        {
            var roundPlayerPoints = new List<(int, int)>();
            // the first time, points are calculated when there is only one record in the _db for the game
            if (dataForRoundPoints.Count() > 1)
            {
                int storyTellerId = dataForRoundPoints.Select(m => m.StorytellerId)
                                                            .FirstOrDefault();
                int stortytellerCardId = dataForRoundPoints.Where(m => m.SelectedByPlayer == storyTellerId)
                                                                 .Select(n => n.CardId)
                                                                 .First();
                int guessersNumbers = dataForRoundPoints.Where(n => n.GuessedCardId != null)
                                                        .Select(m => m.GuessedCardId)
                                                        .Count();
                int storytellerCardsGuessedNumber = dataForRoundPoints.Where(m => m.GuessedCardId == stortytellerCardId)
                                                                            .Count();
                var playersIds = dataForRoundPoints.Select(m => m.SelectedByPlayer)
                                                                 .Distinct();
                bool nobodyGuessed = storytellerCardsGuessedNumber == 0;
                bool everybodyGuessed = storytellerCardsGuessedNumber == guessersNumbers;
                bool storytellerLost = nobodyGuessed || everybodyGuessed;

                foreach (int playerId in playersIds)
                {
                    bool isStoryteller = storyTellerId == playerId;
                    bool playerHasGuessed = dataForRoundPoints.Where(m => m.GuessedCardId == stortytellerCardId
                                                                      && m.SelectedByPlayer == playerId)
                                                              .Any();
                    int playerBasePoints = CalculatePlayerBasePoints(isStoryteller, storytellerLost, playerHasGuessed);
                    int playerExtraPoints = 0;
                    // extra points are calculated only if the storyteller hasn't lost and for players excluding storyteller
                    if (!storytellerLost && !isStoryteller)
                    {
                        int playerSelectedCardId = dataForRoundPoints.Where(m => m.SelectedByPlayer == playerId)
                                                                 .Select(n => n.CardId)
                                                                 .FirstOrDefault();
                        playerExtraPoints = dataForRoundPoints.Where(m => m.GuessedCardId == playerSelectedCardId)
                                                              .Count();
                    }
                    int playerPointsSum = playerBasePoints + playerExtraPoints;
                    var playerData = (player: playerId, playerPoints: playerPointsSum);
                    roundPlayerPoints.Add(playerData);
                }
            }
            return roundPlayerPoints;
        }

        private int CalculatePlayerBasePoints(bool isStoryteller, bool hasStorytellerLost, bool playerHasGuessed)
        {
            if (isStoryteller && hasStorytellerLost)
                return _appSettings.Value.BaseMinPoints;
            if (isStoryteller && !hasStorytellerLost)
                return _appSettings.Value.BaseMaxPoints;
            if (!isStoryteller && hasStorytellerLost)
                return _appSettings.Value.BaseMediumPoints;
            else
                return playerHasGuessed ? _appSettings.Value.BaseMaxPoints : _appSettings.Value.BaseMinPoints;
        }

        private IEnumerable<(int playerId, int totalPoints)> CalculateTotalPoints(IEnumerable<DataForPointsCalculation> dataForPointsCalculations)
        {
            return dataForPointsCalculations.GroupBy(m => m.RoundId)
                                            .OrderByDescending(n => n.Key)
                                            .SkipWhile(group => group.Where(k => k.GuessedCardId == null).Count() > 1)
                                            .Select(group => CalculateRoundPoints(group))
                                            .SelectMany(x => x).GroupBy(n => n.playerId)
                                            .Select(player => ValueTuple.Create(
                                                                             player.Key,
                                                                             player.Sum(k => k.roundPoints)));
        }
    }
}
