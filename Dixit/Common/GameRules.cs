﻿namespace Dixit.Common
{
    public class GameRules
    {
        public int TotalCards { get; set; }
        public int BaseMaxPoints { get; set; }
        public int BaseMediumPoints { get; set; }
        public int BaseMinPoints { get; set; }
        public int ExtraSinglePoints { get; set; }
        public int RequiredPlayersNumber { get; set; }
        public int Seed { get; set; }
        public int WinningPoints { get; set; }
    }
}
