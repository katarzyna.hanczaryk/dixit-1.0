USE [master]
GO
/****** Object:  Database [Dixit]    Script Date: 21.10.2021 08:01:15 ******/
CREATE DATABASE [Dixit]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'Dixit', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL15.MSSQLSERVER\MSSQL\DATA\Dixit.mdf' , SIZE = 8192KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'Dixit_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL15.MSSQLSERVER\MSSQL\DATA\Dixit_log.ldf' , SIZE = 8192KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
 WITH CATALOG_COLLATION = DATABASE_DEFAULT
GO
ALTER DATABASE [Dixit] SET COMPATIBILITY_LEVEL = 150
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [Dixit].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [Dixit] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [Dixit] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [Dixit] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [Dixit] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [Dixit] SET ARITHABORT OFF 
GO
ALTER DATABASE [Dixit] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [Dixit] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [Dixit] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [Dixit] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [Dixit] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [Dixit] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [Dixit] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [Dixit] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [Dixit] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [Dixit] SET  DISABLE_BROKER 
GO
ALTER DATABASE [Dixit] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [Dixit] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [Dixit] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [Dixit] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [Dixit] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [Dixit] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [Dixit] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [Dixit] SET RECOVERY FULL 
GO
ALTER DATABASE [Dixit] SET  MULTI_USER 
GO
ALTER DATABASE [Dixit] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [Dixit] SET DB_CHAINING OFF 
GO
ALTER DATABASE [Dixit] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [Dixit] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [Dixit] SET DELAYED_DURABILITY = DISABLED 
GO
ALTER DATABASE [Dixit] SET ACCELERATED_DATABASE_RECOVERY = OFF  
GO
EXEC sys.sp_db_vardecimal_storage_format N'Dixit', N'ON'
GO
ALTER DATABASE [Dixit] SET QUERY_STORE = OFF
GO
USE [Dixit]
GO
/****** Object:  Table [dbo].[PlayerGuessCards]    Script Date: 21.10.2021 08:01:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PlayerGuessCards](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[RoundChosenCardId] [int] NOT NULL,
	[PlayerId] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[CardsInGame]    Script Date: 21.10.2021 08:01:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CardsInGame](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[CardId] [int] NOT NULL,
	[PlayerId] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Rounds]    Script Date: 21.10.2021 08:01:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Rounds](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[StorytellerId] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[GameRounds]    Script Date: 21.10.2021 08:01:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[GameRounds](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[GameId] [int] NOT NULL,
	[RoundId] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Players]    Script Date: 21.10.2021 08:01:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Players](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](512) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[RoundChosenCards]    Script Date: 21.10.2021 08:01:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[RoundChosenCards](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[RoundId] [int] NOT NULL,
	[CardId] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  View [dbo].[vwDataForPointsCalculation]    Script Date: 21.10.2021 08:01:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[vwDataForPointsCalculation]

AS
	SELECT GameId, r.ID as RoundId, r.StorytellerId, 
	
	rcc.CardId, cg.PlayerId AS SelectedByPlayer, p.[Name], GuessedCardId

	FROM [dbo].[GameRounds] gr
	INNER JOIN [dbo].[Rounds] r ON r.ID = gr.RoundId 
	RIGHT JOIN [dbo].[RoundChosenCards] rcc ON r.ID = rcc.RoundId
	LEFT JOIN [dbo].[CardsInGame] cg ON rcc.CardId = cg.CardId
	LEFT JOIN [dbo].[Players] p ON p.ID = cg.PlayerId
	LEFT JOIN
	(select PlayerId, RoundId, RoundChosenCards.CardId as GuessedCardId 
from RoundChosenCards right join PlayerGuessCards
on RoundChosenCardId = RoundChosenCards.ID) AS guesses
ON cg.PlayerId = guesses.PlayerId AND
	r.ID = guesses.RoundId

GO
/****** Object:  Table [dbo].[CardDescriptions]    Script Date: 21.10.2021 08:01:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CardDescriptions](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Description] [varchar](512) NOT NULL,
	[RoundId] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Cards]    Script Date: 21.10.2021 08:01:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Cards](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[FileName] [varchar](512) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[GamePlayers]    Script Date: 21.10.2021 08:01:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[GamePlayers](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[GameId] [int] NOT NULL,
	[PlayerId] [int] NOT NULL,
	[Order] [tinyint] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Games]    Script Date: 21.10.2021 08:01:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Games](
	[ID] [int] IDENTITY(1,1) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CardDescriptions]  WITH CHECK ADD  CONSTRAINT [FK_CardDescriptions_Round] FOREIGN KEY([RoundId])
REFERENCES [dbo].[Rounds] ([ID])
GO
ALTER TABLE [dbo].[CardDescriptions] CHECK CONSTRAINT [FK_CardDescriptions_Round]
GO
ALTER TABLE [dbo].[CardsInGame]  WITH CHECK ADD  CONSTRAINT [FK_CardID] FOREIGN KEY([CardId])
REFERENCES [dbo].[Cards] ([ID])
GO
ALTER TABLE [dbo].[CardsInGame] CHECK CONSTRAINT [FK_CardID]
GO
ALTER TABLE [dbo].[CardsInGame]  WITH CHECK ADD  CONSTRAINT [FK_PlayerId] FOREIGN KEY([PlayerId])
REFERENCES [dbo].[Players] ([ID])
GO
ALTER TABLE [dbo].[CardsInGame] CHECK CONSTRAINT [FK_PlayerId]
GO
ALTER TABLE [dbo].[GamePlayers]  WITH CHECK ADD  CONSTRAINT [FK_Game_GamePlayer] FOREIGN KEY([GameId])
REFERENCES [dbo].[Games] ([ID])
GO
ALTER TABLE [dbo].[GamePlayers] CHECK CONSTRAINT [FK_Game_GamePlayer]
GO
ALTER TABLE [dbo].[GamePlayers]  WITH CHECK ADD  CONSTRAINT [FK_Player_GamePlayer] FOREIGN KEY([PlayerId])
REFERENCES [dbo].[Players] ([ID])
GO
ALTER TABLE [dbo].[GamePlayers] CHECK CONSTRAINT [FK_Player_GamePlayer]
GO
ALTER TABLE [dbo].[GameRounds]  WITH CHECK ADD  CONSTRAINT [FK_Game_GameRounds] FOREIGN KEY([GameId])
REFERENCES [dbo].[Games] ([ID])
GO
ALTER TABLE [dbo].[GameRounds] CHECK CONSTRAINT [FK_Game_GameRounds]
GO
ALTER TABLE [dbo].[GameRounds]  WITH CHECK ADD  CONSTRAINT [FK_Round_GameRounds] FOREIGN KEY([RoundId])
REFERENCES [dbo].[Rounds] ([ID])
GO
ALTER TABLE [dbo].[GameRounds] CHECK CONSTRAINT [FK_Round_GameRounds]
GO
ALTER TABLE [dbo].[PlayerGuessCards]  WITH CHECK ADD  CONSTRAINT [FK_PlayerGuessCards_PlayerId] FOREIGN KEY([PlayerId])
REFERENCES [dbo].[Players] ([ID])
GO
ALTER TABLE [dbo].[PlayerGuessCards] CHECK CONSTRAINT [FK_PlayerGuessCards_PlayerId]
GO
ALTER TABLE [dbo].[PlayerGuessCards]  WITH CHECK ADD  CONSTRAINT [FK_PlayerGuessCards_RoundChosenCardId] FOREIGN KEY([RoundChosenCardId])
REFERENCES [dbo].[RoundChosenCards] ([ID])
GO
ALTER TABLE [dbo].[PlayerGuessCards] CHECK CONSTRAINT [FK_PlayerGuessCards_RoundChosenCardId]
GO
ALTER TABLE [dbo].[RoundChosenCards]  WITH CHECK ADD  CONSTRAINT [FK_RoundChocenCards_CardId] FOREIGN KEY([CardId])
REFERENCES [dbo].[Cards] ([ID])
GO
ALTER TABLE [dbo].[RoundChosenCards] CHECK CONSTRAINT [FK_RoundChocenCards_CardId]
GO
ALTER TABLE [dbo].[RoundChosenCards]  WITH CHECK ADD  CONSTRAINT [FK_RoundChosenCards_RoundId] FOREIGN KEY([RoundId])
REFERENCES [dbo].[Rounds] ([ID])
GO
ALTER TABLE [dbo].[RoundChosenCards] CHECK CONSTRAINT [FK_RoundChosenCards_RoundId]
GO
ALTER TABLE [dbo].[Rounds]  WITH CHECK ADD  CONSTRAINT [FK_StorytellerId] FOREIGN KEY([StorytellerId])
REFERENCES [dbo].[Players] ([ID])
GO
ALTER TABLE [dbo].[Rounds] CHECK CONSTRAINT [FK_StorytellerId]
GO
/****** Object:  StoredProcedure [dbo].[spAddNewGame]    Script Date: 21.10.2021 08:01:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[spAddNewGame]
@PlayerId int,
@NewGameId int OUT
AS
BEGIN
INSERT INTO [dbo].[Games] DEFAULT VALUES
SET @NewGameId = SCOPE_IDENTITY()
END
INSERT INTO [dbo].[GamePlayers] (GameId, PlayerId, [Order])
VALUES (@NewGameId, @PlayerId, 1)
RETURN @NewGameId
GO
/****** Object:  StoredProcedure [dbo].[spAddNewPlayer]    Script Date: 21.10.2021 08:01:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[spAddNewPlayer]
@Name varchar(512),
@Identity int OUT
AS
INSERT INTO [dbo].[Players] (Name)
VALUES (@Name)
SET @Identity = SCOPE_IDENTITY()
RETURN @Identity
GO
/****** Object:  StoredProcedure [dbo].[spAddNewRound]    Script Date: 21.10.2021 08:01:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[spAddNewRound]
@StorytellerId int
,@GameId int
AS
BEGIN
	BEGIN TRANSACTION;
	SAVE TRANSACTION MySavePoint;
	BEGIN TRY
		DECLARE
			@RoundId int	
		INSERT INTO [dbo].[Rounds] 
			(StorytellerId) VALUES (@StorytellerId)
		SET @RoundId = SCOPE_IDENTITY()
		INSERT INTO [dbo].[GameRounds] 
		(
		GameId
		,RoundId
		)
		VALUES
		(
		@GameId
		,@RoundId
		);
		COMMIT TRANSACTION
	END TRY
	BEGIN CATCH
		IF @@TRANCOUNT > 0
		BEGIN
			ROLLBACK TRANSACTION MySavePoint;
		END
	END CATCH
END;
GO
/****** Object:  StoredProcedure [dbo].[spAddNewRoundAndNewStoryteller]    Script Date: 21.10.2021 08:01:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[spAddNewRoundAndNewStoryteller]
@PlayerId int
,@GameId int
,@CurrentRoundId int out
AS
BEGIN
	BEGIN TRANSACTION;
	SAVE TRANSACTION MySavePoint;
	BEGIN TRY
	/*** Add check if new round has been already added, if so - do nothing***/
			DECLARE
				@RoundId int
			SET @RoundId = (SELECT TOP 1 ID FROM Rounds ORDER BY ID DESC) /**roundId at the beginning**/
			IF EXISTS (SELECT 1 FROM CardDescriptions WHERE RoundId = @RoundId) /**new round hasn't been added yet**/
			BEGIN
				DECLARE
				@NewStorytellerId int
				,@StorytellerOrder int
			SELECT @StorytellerOrder = [Order] FROM GamePlayers WHERE PlayerId IN (SELECT StorytellerId FROM Rounds WHERE ID = @RoundId)
			IF EXISTS (SELECT 1 FROM GamePlayers WHERE [Order] = @StorytellerOrder + 1 AND GameId = @GameId)
				SELECT @NewStorytellerId = PlayerId FROM GamePlayers WHERE [Order] = @StorytellerOrder + 1 AND GameId = @GameId
			ELSE
				SELECT @NewStorytellerId = PlayerId FROM GamePlayers WHERE [Order] = 1 AND GameId = @GameId
			INSERT INTO [dbo].[Rounds] 
				(StorytellerId) VALUES (@NewStorytellerId)
			SET @CurrentRoundId = SCOPE_IDENTITY()
			INSERT INTO [dbo].[GameRounds] 
			(
			GameId
			,RoundId
			)
			VALUES
			(
			@GameId
			,@CurrentRoundId
			);
			END
			ELSE
			BEGIN
				SET @CurrentRoundId = @RoundId /**new round added already - current round is the input round**/
			END
		COMMIT TRANSACTION
	END TRY
	BEGIN CATCH
		IF @@TRANCOUNT > 0
		BEGIN
			ROLLBACK TRANSACTION MySavePoint;
		END
	END CATCH
END;
GO
/****** Object:  StoredProcedure [dbo].[spAssignPlayerToGame]    Script Date: 21.10.2021 08:01:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[spAssignPlayerToGame]
@PlayerId int
,@GameIdToAssign int OUT
,@RoundId int OUT
AS
BEGIN
	BEGIN TRANSACTION;
	SAVE TRANSACTION MySavePoint;
		BEGIN TRY
			DECLARE @PlayerName varchar(32)
			SELECT @PlayerName = p.Name FROM Players p LEFT JOIN GamePlayers gp ON gp.PlayerId = p.ID WHERE p.ID = @PlayerId
			SET @GameIdToAssign = (SELECT TOP 1 incompleteGames.GameId 
								  FROM (SELECT GameId 
										FROM GamePlayers 
										GROUP BY GameId 
										HAVING COUNT(*) < 4) AS incompleteGames
								  INNER JOIN (SELECT ID 
											  FROM Games
											  EXCEPT (SELECT gp.GameId 
													  FROM GamePlayers gp 
													  LEFT JOIN Players p
													  ON p.ID = gp.PlayerId 
													  WHERE p.Name = @PlayerName)) AS gamesWithoutGivenPlayer 
								  ON incompleteGames.GameId = gamesWithoutGivenPlayer.ID)
			/**** when no existing game meeting conditions can be found, the new game needs to be added ****/
			IF (@GameIdToAssign IS NULL)
				BEGIN
					INSERT INTO [dbo].[Games] DEFAULT VALUES
					SET @GameIdToAssign = SCOPE_IDENTITY()
					INSERT INTO [dbo].[Rounds]
					(
					StorytellerId
					) VALUES 
					(
					@PlayerId
					)
					SET @RoundId = SCOPE_IDENTITY()
					INSERT INTO [dbo].[GameRounds]
					(
					GameId
					,RoundId
					)
					VALUES 
					(
					@GameIdToAssign
					,@RoundId
					);
					INSERT INTO [dbo].[GamePlayers] 
					(
					GameId
					,PlayerId
					,[Order]
					)
					VALUES 
					(
					@GameIdToAssign
					,@PlayerId
					,1
					)
				END
			ELSE 
				BEGIN
					SET @RoundId = (SELECT TOP 1 RoundId FROM GameRounds WHERE GameId = @GameIdToAssign)
					DECLARE @OrderToAssign int
					SET @OrderToAssign = ((SELECT TOP 1 [Order] FROM GamePlayers WHERE GameId = @GameIdToAssign ORDER BY ID DESC) + 1)
					INSERT INTO [dbo].[GamePlayers] 
					(
					GameId
					,PlayerId
					,[Order]
					)
					VALUES 
					(
					@GameIdToAssign
					,@PlayerId
					,@OrderToAssign
					)
				END		
	COMMIT TRANSACTION;
		RETURN @GameIdToAssign
		RETURN @RoundId
		END TRY
		BEGIN CATCH
			IF @@TRANCOUNT > 0
				BEGIN
					ROLLBACK TRANSACTION MySavePoint;
				END
		END CATCH
END;
GO
/****** Object:  StoredProcedure [dbo].[spRetrieveDataForPointsCalculation]    Script Date: 21.10.2021 08:01:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[spRetrieveDataForPointsCalculation]
@GameId int

AS
	BEGIN
		SELECT r.ID as RoundId, StorytellerId, rcc.CardId, cg.PlayerId AS SelectedByPlayer, pgc.PlayerId as GuesserId
		FROM [dbo].[GameRounds] gr
		INNER JOIN [dbo].[Rounds] r ON r.ID = gr.RoundId 
		LEFT JOIN [dbo].[RoundChosenCards] rcc ON r.ID = rcc.RoundId
		LEFT JOIN  [dbo].[PlayerGuessCards] pgc ON rcc.ID = pgc.RoundChosenCardId
		LEFT JOIN [dbo].[CardsInGame] cg ON rcc.CardId = cg.CardId
		WHERE gr.GameId = @GameId
	END
GO
USE [master]
GO
ALTER DATABASE [Dixit] SET  READ_WRITE 
GO
